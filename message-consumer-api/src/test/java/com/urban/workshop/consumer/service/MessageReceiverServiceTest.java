package com.urban.workshop.consumer.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.google.gson.Gson;
import com.urban.workshop.consumer.dto.MessageDTO;

public class MessageReceiverServiceTest {

	private MessageReceiverService messageReceiverService = new MessageReceiverService();
	
	private MessageService mockedMessageService;
	
	@Before
	public void setup(){
		mockedMessageService = Mockito.mock(MessageService.class);
		ReflectionTestUtils.setField(messageReceiverService, "messageService", mockedMessageService);
	}
	
	@Test
	public void receiveMessageTest(){
		
		String message = "{\"id\":\"1\",\"subject\":\"Test\",\"message\":\"Hello\"}";
		Gson gson = new Gson();
		MessageDTO messageDTO = gson.fromJson(message, MessageDTO.class);
		messageReceiverService.receiveMessage(message);
		
		Mockito.verify(mockedMessageService).store(messageDTO);
	}
}
