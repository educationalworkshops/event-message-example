package com.urban.workshop.consumer.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.urban.workshop.consumer.dto.MessageDTO;
import com.urban.workshop.consumer.repository.MessageRepository;

public class MessageServiceTest {

	MessageService messageService = new MessageService();
	MessageRepository mockedMessageRepo;
	
	@Before
	public void setup(){
		mockedMessageRepo = Mockito.mock(MessageRepository.class);
		ReflectionTestUtils.setField(messageService, "messageRepo", mockedMessageRepo);
	}
	
	
	@Test
	public void storeTest(){
		MessageDTO messageDTO = new MessageDTO();
		messageDTO.setId("1");
		messageDTO.setMessage("Hello");
		messageDTO.setSubject("Test");
		
		messageService.store(messageDTO);
		
		Mockito.verify(mockedMessageRepo);
	}
}
