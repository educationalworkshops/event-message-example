package com.urban.workshop.consumer.service;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.urban.workshop.consumer.dto.MessageDTO;

/**
 * Messages receiver service that monitors a kafka stream for message events
 * 
 * @author hernan.urban
 *
 */
@Service
public class MessageReceiverService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageReceiverService.class);

	private CountDownLatch latch = new CountDownLatch(1);

	private Gson gson = new Gson();

	@Autowired
	private MessageService messageService;

	/**
	 * Message listener that writes on DB when a message event is received.
	 * 
	 * @param message
	 *            the message event
	 */
	@KafkaListener(topics = "${kafka.config.message.topic}")
	public void receiveMessage(String message) {
		LOGGER.info("Received message='{}'", message);
		MessageDTO messageDTO = gson.fromJson(message, MessageDTO.class);
		messageService.store(messageDTO);
		LOGGER.info("Stored message='{}'", message);
		latch.countDown();
	}

	/**
	 * Retrieves the count down latch
	 * 
	 * @return The count down latch
	 */
	public CountDownLatch getLatch() {
		return latch;
	}
}
