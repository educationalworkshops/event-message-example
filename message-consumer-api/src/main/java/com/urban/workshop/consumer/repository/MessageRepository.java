package com.urban.workshop.consumer.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.urban.workshop.consumer.domain.Message;

/**
 * Message Repository
 * 
 * @author hernan.urban
 *
 */
@Repository
public interface MessageRepository extends CassandraRepository<Message> {

	/**
	 * Retrieves an optional of Message for a given MessageId.
	 * 
	 * @param messageId
	 *            the Message ID
	 * @return an Optional of Message
	 */
	public Optional<Message> findByMessageId(String messageId);

	/**
	 * Retrieves a list of Messages for a given date range.
	 * 
	 * @param from
	 *            date from
	 * @param to
	 *            date to
	 * @return a list of messages
	 */
	public List<Message> findByCreationDateBetween(Date from, Date to);
}
