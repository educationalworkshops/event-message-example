package com.urban.workshop.consumer.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Message Data Transfer Object.
 * 
 * @author hernan.urban
 *
 */
public class MessageDTO implements Serializable {

	private static final long serialVersionUID = 6546276099700629289L;

	private String id;
	private String subject;
	private String message;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(id).append(subject).append(message).toHashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof MessageDTO)) {
			return false;
		}
		MessageDTO messageDTO = (MessageDTO) o;
		return new EqualsBuilder().append(id, messageDTO.id).append(subject, messageDTO.subject)
				.append(message, messageDTO.message).isEquals();
	}

}
