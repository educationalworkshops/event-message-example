package com.urban.workshop.consumer.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.utils.UUIDs;
import com.urban.workshop.consumer.domain.Message;
import com.urban.workshop.consumer.dto.MessageDTO;
import com.urban.workshop.consumer.repository.MessageRepository;

/**
 * Persistent message service
 * 
 * @author hernan.urban
 *
 */
@Service
public class MessageService {

	@Autowired
	MessageRepository messageRepo;

	/**
	 * Saves in DB the given message
	 * 
	 * @param messageDTO
	 *            the message DTO to be stored.
	 */
	public void store(MessageDTO messageDTO) {
		Message message = convertMessage(messageDTO);
		messageRepo.save(message);
	}

	/**
	 * Converts a given message DTO in a domain message.
	 * 
	 * @param messageDTO
	 *            the message DTO.
	 * @return a domain message
	 */
	private Message convertMessage(MessageDTO messageDTO) {
		Message message = new Message();
		message.setCreationDate(new Date());
		message.setId(UUIDs.timeBased());
		message.setMessageId(messageDTO.getId());
		message.setMessage(messageDTO.getMessage());
		message.setSubject(messageDTO.getSubject());
		return message;
	}
}
