package com.urban.workshop.consumer;

import org.springframework.boot.SpringApplication;

import com.urban.workshop.consumer.config.MessageReceiverAPIConfiguration;

/**
 * Message receiver application
 * 
 * @author hernan.urban
 *
 */
public class MessageConsumerApplication {
	public static void main(String[] args) {
		SpringApplication.run(MessageReceiverAPIConfiguration.class, args);
	}
}
