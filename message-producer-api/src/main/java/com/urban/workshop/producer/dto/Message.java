package com.urban.workshop.producer.dto;

import java.io.Serializable;

/**
 * Message Data Transfer Object.
 * 
 * @author hernan.urban
 *
 */
public class Message implements Serializable {

	private static final long serialVersionUID = 6546276099700629283L;

	private String id;
	private String subject;
	private String message;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
