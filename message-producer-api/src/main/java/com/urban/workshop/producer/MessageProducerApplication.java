package com.urban.workshop.producer;

import org.springframework.boot.SpringApplication;

import com.urban.workshop.producer.config.MessageProducerAPIConfiguration;

/**
 * Message Producer Application
 * 
 * @author hernan.urban
 *
 */
public class MessageProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessageProducerAPIConfiguration.class, args);
	}
}
