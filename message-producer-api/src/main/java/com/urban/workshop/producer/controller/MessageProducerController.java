package com.urban.workshop.producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.urban.workshop.producer.dto.Message;
import com.urban.workshop.producer.service.MessageSenderService;

/**
 * Rest API to publish messages.
 * 
 * @author hernan.urban
 *
 */
@RestController
public class MessageProducerController {

	@Autowired
	MessageSenderService messageService;

	@RequestMapping(path = "/publish", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Message publishMessage(@RequestBody Message message) {
		messageService.sendMessage(message);
		return message;
	}

}
