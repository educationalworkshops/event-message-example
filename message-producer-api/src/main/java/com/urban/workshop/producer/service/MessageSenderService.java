package com.urban.workshop.producer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.google.gson.Gson;
import com.urban.workshop.producer.dto.Message;

/**
 * Message sender service
 * 
 * @author hernan.urban
 *
 */
@Service
public class MessageSenderService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageSenderService.class);

	@Value("${kafka.config.message.topic}")
	private String topic;

	@Autowired
	private KafkaTemplate<Integer, String> kafkaTemplate;

	/**
	 * Sends a message asynchronously through Kafka stream. This method
	 * retrieves a future once the message is published and log the result.
	 * 
	 * @param messageDTO
	 *            the message to send
	 */
	public void sendMessage(Message messageDTO) {

		Gson gson = new Gson();
		String message = gson.toJson(messageDTO);

		ListenableFuture<SendResult<Integer, String>> future = kafkaTemplate.send(topic, message);

		future.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
			@Override
			public void onSuccess(SendResult<Integer, String> result) {
				LOGGER.info("sent message='{}' with offset={}", message, result.getRecordMetadata().offset());
			}

			@Override
			public void onFailure(Throwable ex) {
				LOGGER.error("unable to send message='{}'", message, ex);
			}
		});
	}
}