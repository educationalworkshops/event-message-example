package com.urban.workshop.producer.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.urban.workshop.producer.dto.Message;
import com.urban.workshop.producer.service.MessageSenderService;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for MessageProducerController
 * 
 * @author hernan.urban
 *
 */
public class MessageProducerControllerTest {

	private MessageProducerController messageProducerController = new MessageProducerController();
	private MessageSenderService mockedMessageSenderService;

	@Before
	public void setup() {
		mockedMessageSenderService = Mockito.mock(MessageSenderService.class);
		ReflectionTestUtils.setField(messageProducerController, "messageService", mockedMessageSenderService);
	}
	
	@Test
	public void publishMessageTest(){
		Message message = new Message();
		message.setId("1");
		message.setMessage("Hello");
		message.setSubject("Test");
		
		Message response = messageProducerController.publishMessage(message);
		
		Mockito.verify(mockedMessageSenderService).sendMessage(message);
		assertEquals(response, message);
	}
}
