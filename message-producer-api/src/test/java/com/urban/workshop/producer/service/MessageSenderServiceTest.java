package com.urban.workshop.producer.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.concurrent.ListenableFuture;

import com.google.gson.Gson;
import com.urban.workshop.producer.dto.Message;

/**
 * Unit test for MessageSenderService
 * 
 * @author hernan.urban
 *
 */
public class MessageSenderServiceTest {

	private MessageSenderService messageSenderService = new MessageSenderService();

	private KafkaTemplate<Integer, String> mockedKafkaTemplate;
	private String topic = "1";

	@Before
	@SuppressWarnings("unchecked")
	public void setup() {
		mockedKafkaTemplate = Mockito.mock(KafkaTemplate.class);
		ReflectionTestUtils.setField(messageSenderService, "kafkaTemplate", mockedKafkaTemplate);
		ReflectionTestUtils.setField(messageSenderService, "topic", topic);
	}

	/**
	 * Test for success messageSenderService.sendMessage method
	 */
	@Test
	public void sendMessageTest() {
		Message messageDTO = new Message();
		messageDTO.setId("1");
		messageDTO.setMessage("Hello");
		messageDTO.setSubject("Test");
		Gson gson = new Gson();
		String data = gson.toJson(messageDTO);

		@SuppressWarnings("unchecked")
		ListenableFuture<SendResult<Integer, String>> future = Mockito.mock(ListenableFuture.class);
		Mockito.when(mockedKafkaTemplate.send(topic, data)).thenReturn(future);
		messageSenderService.sendMessage(messageDTO);

		// Verifies message sent
		Mockito.verify(mockedKafkaTemplate).send(topic, data);

	}

}
